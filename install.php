<?php require_once 'install_files/backend/boot.php'; ?>
<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width">
		<title>ZEngine installer</title>
		<link href="install_files/css/app.css" rel="stylesheet">
	</head>
	<body class="font-body">
		<div id="app">
			<App></App>
		</div>
		<script src="install_files/js/app.js"></script>
	</body>
</html>
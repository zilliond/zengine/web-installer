<?php


class Downloader
{
    const BASE_URL = "https://zillion.eu-central-1.linodeobjects.com/";
    const RELEASES_URL = self::BASE_URL . "zengine/releases/";
    const LATEST_FULL_FILENAME = "latest-full.zip";
    const LATEST_DEV_FULL_FILENAME = "dev-latest-full.zip";
    
    
    protected $is_debug = false;
    protected $is_dev = false;
    
    public function __construct(Logger $logger, string $temp_path)
    {
        global $isDebug, $isDev;
        $this->is_debug = isset($isDebug) && $isDebug;
        $this->is_dev = isset($isDev) && $isDev;
        $this->logger = $logger;
        $this->temp_path = $temp_path;
    }
    
    public function download_latest_release()
    {
        $url = self::RELEASES_URL . ($this->is_dev ? self::LATEST_DEV_FULL_FILENAME : self::LATEST_FULL_FILENAME);
        $this->logger->info('Downloader: download file url '. $url);
        $filepath = $this->temp_path . DIRECTORY_SEPARATOR . 'release.zip';
        $fp = fopen($filepath, 'w+b');
        $curl = $this->download_request_prepare($url, $fp);
        $this->download_progress($curl);
        $this->logger->info('Downloader: start download latest full release ', [
            'filepath' 		=> $filepath,
            'status_code' 	=> $this->response_status_code($curl),
            'curl_info' 	=> $this->curl_info($curl),
        ]);
        $this->download_request_exec($curl);
        $this->logger->info('Downloader: downloading latest full release ', [
            'filepath' 		=> $filepath,
            'status_code' 	=> $this->response_status_code($curl),
            'curl_info' 	=> $this->curl_info($curl),
            'progress'		=> $_SESSION['download_percentage']
        ]);
        fclose($fp);
    }
    
    public function ping() : bool
    {
        $curl = $this->request_prepare(self::BASE_URL);
        $this->request_exec($curl);
        $status_code = $this->response_status_code($curl);
        if($this->is_debug) {
            $this->logger->info('Zillion storage ping', [
                'status_code' => $this->response_status_code($curl), 'curl_errno' => (int) curl_errno($curl),
                'curl_error' => (string) curl_error($curl)
            ]);
        }
        return curl_errno($curl) === 0 && $status_code === 403;
    }
    
    protected function download_progress($curl)
    {
        $_SESSION['download_percentage'] = 0.0; //initialize it
        curl_setopt( $curl, CURLOPT_PROGRESSFUNCTION, 'progress' );
        curl_setopt( $curl, CURLOPT_NOPROGRESS, false );
        function progress($resource,$download_size, $downloaded, $upload_size, $uploaded){
            $percentage= $download_size === 0 ? 0.0 : ( ($downloaded/$download_size) * 100);
            session_start();
            $_SESSION['download_percentage'] = $percentage;
            session_write_close();
        }
    }
    
    protected function download_request_prepare($url, $fp)
    {
        $curl = $this->request_prepare($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 0);
        curl_setopt($curl, CURLOPT_TIMEOUT, 50);
        curl_setopt($curl, CURLOPT_FILE, $fp);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        return $curl;
    }
    
    protected function curl_info($curl)
    {
        return curl_getinfo($curl);
    }
    
    protected function response_status_code($curl)
    {
        return curl_getinfo($curl, CURLINFO_RESPONSE_CODE);
    }
    
    protected function download_request_exec($curl)
    {
        curl_exec($curl);
        curl_close($curl);
    }
    
    protected function request_exec($curl)
    {
        $res = curl_exec($curl);
        return json_decode($res, true);
    }
    
    protected function request_prepare($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_URL, $url);
        return $curl;
    }
}
<?php


class Installer
{
	/** @var ConfigRewriter Laravel config rewriter */
	protected $rewriter;
	
	protected $base_path;
	
	protected $temp_path;
	
	protected $config_path;
	
	protected $installer_files_path;
	protected $log_file;
	
	
	/** @var Illuminate\Foundation\Application */
	protected $engine;
	
	/** @var \Modules\Core\Console\ZEngineKernel */
	protected $engine_kernel;
	
	/** @var Logger */
	protected $logger;
	
	/** @var Downloader */
	protected $downloader;
	
	public function __construct()
	{
		$this->rewriter = new ConfigRewriter;
		
		$this->base_path = BASE_PATH;
		$this->installer_files_path = BASE_PATH . DIRECTORY_SEPARATOR . 'install_files';
		$this->temp_path = BASE_PATH . DIRECTORY_SEPARATOR . 'install_files';
		$this->config_path = BASE_PATH . DIRECTORY_SEPARATOR . 'config';
		$this->initLogger();
		$this->downloader = new Downloader($this->logger, $this->temp_path);
		$this->initPostJson();
		$this->logPostRequest();
		
		$this->handle();
	}
	
	/**
	 * Handle requests
	 */
	protected function handle()
	{
		if($handler = $this->post('handler')){
			try {
				if (!preg_match('/^on[A-Z]{1}[\w+]*$/', $handler))
					throw new Exception('Bad handler: '. $this->e($handler));
				
				if(method_exists($this, $handler)){
					$this->info('Handler exist: '. $handler);
					$result = $this->$handler();
					$this->info('Handler result', $result);
					header('Content-Type: application/json');
					die(json_encode($result));
				}
			}catch (InstallerException $exception){
				header('HTTP/1.1 500 Internal Server Error', true, 500);
				$this->info('Handler error', $exception->getMessage());
				$this->info('Error trace log', $exception->getTraceAsString());
				$errorMsg = htmlspecialchars_decode(strip_tags($exception->getMessage()));
				die(json_encode([
					'status'	=> 'error',
					'message'	=> $errorMsg
				]));
			}
		}
	}
	
	public function onInstall() : array
	{
		$action = $this->post('action');
		$data = $this->post('data', null);
		if(!$action){
			throw new InstallerException('Action not find');
		}
		$this->info('Install action ' . $action, $data);
		$result = false;
		switch ($action) {
			case 'downloadEngine':
				if(!file_exists($this->temp_path . DIRECTORY_SEPARATOR . 'release.zip')){
					$this->downloader->download_latest_release();
				}
				$result = [
					'status' => 'success'
				];
				break;
			case 'downloadProgress':
				$result = [
					'progress'  => $_SESSION['download_percentage'] ?? 'error',
					'status'	=> (isset($_SESSION['download_percentage']) && $_SESSION['download_percentage'] === 100) ? 'success' : 'progress',
				];
				break;
			case 'extractEngine':
				if(!file_exists($this->base_path . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'index.php')){
					$result = $this->extractEngine();
				}else{
					$result = ['status' => 'success'];
				}
				break;
			case 'configDatabase':
				$result = $this->configDatabase();
				break;
			case 'configEngine':
				$result = $this->configEngine();
				break;
			case 'setupDatabase':
				$result = $this->setupDatabase();
				break;
			case 'createAdmin':
				$result = $this->createAdmin();
				break;
			case 'cleanInstaller':
				$result = $this->cleanInstaller();
				break;
		}
		$this->info("Action $action result ", $result);
		return $result;
	}
	
	public function cleanInstaller()
	{
		$this->info('cleanInstaller');
		if(is_dir($this->base_path . DIRECTORY_SEPARATOR . 'src')){
			$this->recursiveDeleteFolder($this->base_path . DIRECTORY_SEPARATOR . 'src');
		}
		unlink($this->base_path . DIRECTORY_SEPARATOR . 'install.php');
		$this->recursiveDeleteFolder($this->temp_path);
		// Need die, prevent logging
		die(json_encode([
			'status' => 'success'
		]));
		
	}
	
	protected function recursiveDeleteFolder($path)
	{
		$it = new RecursiveDirectoryIterator($path, RecursiveDirectoryIterator::SKIP_DOTS);
		$files = new RecursiveIteratorIterator($it,
			RecursiveIteratorIterator::CHILD_FIRST);
		foreach($files as $file) {
			if ($file->isDir()){
				rmdir($file->getRealPath());
			} else {
				unlink($file->getRealPath());
			}
		}
		rmdir($path);
	}
	
	public function createAdmin()
	{
		$data = $this->post('data');
		$admin = [
			'ADMIN_NAME' => $data['admin_name'],
			'ADMIN_EMAIL' => $data['admin_email'],
			'ADMIN_LOGIN' => $data['admin_login'],
			'ADMIN_PASSWORD' => $data['admin_password'],
		];
		$this->putEnv($admin);
		$this->bootEngine();
		$input = new Symfony\Component\Console\Input\ArrayInput([
			'command' => 'db:seed',
			'--force' => true,
			'--class' => '\Modules\Core\Database\Seeders\InstallAdminSeeder',
		]);
		$result = $this->runArtisanCommand($input);
		$this->info('createAdmin', [
			'status' 	=> $result['status'],
			'output'	=> $result['output']
		]);
		//$this->deleteEnv(array_keys($admin));
		//TODO: check valid status
		return [
			'status' 	=> 'success',
			'output'	=> $result['output']
		];
	}
	
	public function setupDatabase()
	{
		$this->bootEngine();
		$input = new Symfony\Component\Console\Input\ArrayInput([
			'command' => 'migrate',
			'--force' => true,
		]);
		$result = $this->runArtisanCommand($input);
		$this->info('setupDatabase', [
			'status' 	=> $result['status'],
			'output'	=> $result['output']
		]);
		//TODO: check valid status
		return [
			'status' 	=> 'success',
			'output'	=> $result['output']
		];
	}
	
	protected function runArtisanCommand($input){
		
		$status = $this->engine_kernel->handle(
			$input,
			$output = new \Symfony\Component\Console\Output\BufferedOutput()
		);
		$this->engine_kernel->terminate($input, $status);
		$output = $output->fetch();
		$this->info('runArtisan command', [
			'input'  => $input,
			'output' => $output
		]);
		return [
			'status'	=> $status,
			'output'	=> $output
		];
	}
	
	public function configDatabase()
	{
		$data = $this->post('data');
		$db_config = [
			'DB_CONNECTION' 	=> 'mysql',
			'DB_HOST' 			=> $data['db_host'] ?? 'localhost',
			'DB_PORT' 			=> $data['db_post'] ?? 3306,
			'DB_DATABASE' 		=> $data['db_name'],
			'DB_USERNAME' 		=> $data['db_user'],
			'DB_PASSWORD' 		=> $data['db_password'],
		];
		$this->putEnv($db_config);
		return [
			'status' => 'success'
		];
	}
	
	public function configEngine()
	{
		$data = $this->post('data');
		$env_config = [
			'APP_NAME' 			=> $data['engine_name'],
			'APP_KEY' 			=> $data['engine_key'],
			'APP_URL'			=> $this->getBaseUrl(),
			'BALANCES_TYPE' 	=> $data['engine_balances'],
		];
		if( isset($data['engine_licence']) && !empty($data['engine_licence']) ){
		    $this->setLicence($data['engine_licence']);
        }
		$this->putEnv($env_config);
		return [
			'status' => 'success'
		];
	}
	
	public function setLicence($licence)
    {
        $domain = $_SERVER['HTTP_HOST'];
        $filename = "$domain.key";
        file_put_contents("storage/app/$filename", $licence);
    }
	
	protected function putEnv(array $data)
	{
		$env_file = $this->base_path . DIRECTORY_SEPARATOR . '.env';
		$env_example_file = $this->base_path . DIRECTORY_SEPARATOR . '.env.example';
		if(!file_exists($this->base_path . DIRECTORY_SEPARATOR . '.env')){
			if(file_exists($this->base_path . DIRECTORY_SEPARATOR . '.env.example')){
				$this->info('.env.example copied to .env');
				copy($env_example_file, $env_file);
			}else{
				throw new InstallerException('.env and .env.example files not find');
			}
		}
		$env = file_get_contents($env_file);
		foreach ($data as $key => $value){
			$key_regex = "/^$key=(.+)?$/m";
			if(strpos($value, ' ') !== false){
				$value = '"'. $value . '"';
			}
			if(preg_match($key_regex, $env, $matches)){
				$this->info("env $key : $matches[0]");
				$env = preg_replace($key_regex, $key.'='.$value, $env);
			}else{
				$env .= PHP_EOL.$key.'='.$value;
			}
		}
		file_put_contents($env_file, $env);
	}
	
	protected function deleteEnv(array $keys)
	{
		$env_file = $this->base_path . DIRECTORY_SEPARATOR . '.env';
		$env_example_file = $this->base_path . DIRECTORY_SEPARATOR . '.env.example';
		if(!file_exists($this->base_path . DIRECTORY_SEPARATOR . '.env')){
			throw new InstallerException('.env file not find');
		}
		$env = file_get_contents($env_file);
		foreach ($keys as $key){
			$key_regex = "/^$key=(.+)?$/m";
			if(preg_match($key_regex, $env, $matches)){
				$this->info("env $key : $matches[0]");
				$env = preg_replace($key_regex, '', $env);
			}
		}
		file_put_contents($env_file, $env);
	}
	
	public function extractEngine()
	{
		$release = $this->temp_path . DIRECTORY_SEPARATOR . 'release.zip';
		$destination = $this->base_path;
		$this->info("Extracting file $release to $destination");
		
		$zip = new ZipArchive;
		if(true !== ($status = $zip->open($release))){
			if($status === ZipArchive::ER_NOENT){
				throw new InstallerException('ZipArchive open error: Release file not find');
			}
			throw new InstallerException('ZipArchive open error: '.$status);
		}
		if(true !== ($status = $zip->extractTo($destination))){
			throw new InstallerException('ZipArchive extractTo error: '.$status);
		}
		if(true !== $zip->close()){
			throw new InstallerException('ZipArchive close error');
		}
		
		// TODO: Move public to root
		if (!file_exists($this->base_path . '/public/index.php')
			|| !is_dir($this->base_path . '/Modules')
			|| !is_dir($this->base_path . '/vendor')) {
            throw new InstallerException('extractEngine undefined error');
        }
		
		return [
			'status' => 'success'
		];
	}
	
	public function onCheckDatabaseConnection() : array
	{
		$payload = $this->post('payload');
		$host = $payload['db_host'] ?: 'localhost';
		$port = $payload['db_port'] ?: 3306;
		$name = $payload['db_name'];
		$user = $payload['db_user'];
		$password = $payload['db_password'];
		
		
		$dsn = 'mysql:host='.$host.';dbname='.$name.';port='.$port;
		$this->info($dsn);
		$this->info($user);
		$this->info($password);
		try {
			$db = new PDO($dsn, $user, $password, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
			$query = $db->query('show tables', PDO::FETCH_NUM);
			$result = $query->fetch();
		}catch (PDOException $ex) {
			throw new InstallerException('Connection failed: ' . $ex->getMessage());
		}
		if($result !== false){
			throw new InstallerException(sprintf('Database "%s" is not empty. Truncate the database or use another', $this->e($name)));
		}
		return [
			'status' 	=> true,
			'message' 	=> 'Connection success and database clear'
		];
	}
	
	/** @noinspection PhpUnused */
	public function onCheckRequirements() : array
	{
		$type = $this->post('payload');
		$this->info('check requirement ' . $type);
		$result = false;
		switch ($type) {
			case 'php':
				$result = version_compare(strtolower(trim(PHP_VERSION)), REQUIRED_PHP_VERSION, '>=');
				break;
			case 'ioncube':
				$result = extension_loaded('ionCube Loader');
				break;
			case 'curl':
				$result = function_exists('curl_version');
				break;
			case 'json':
				$result = function_exists('json_decode');
				break;
			case 'pdo':
				$result = defined('PDO::ATTR_DRIVER_NAME');
				break;
			case 'mbstring':
				$result = extension_loaded('mbstring');
				break;
			case 'fileinfo':
				$result = extension_loaded('fileinfo');
				break;
			case 'openssl':
				$result = extension_loaded('openssl');
				break;
			case 'gd':
				$result = extension_loaded('gd');
				break;
			case 'zip':
				$result = class_exists('ZipArchive');
				break;
			case 'hash':
				$result = extension_loaded('hash');
				break;
			case 'soap':
				$result = extension_loaded('soap');
				break;
			case 'zillion_storage':
				$result = $this->downloader->ping();
				break;
		}
		$this->info("Requirement $type ".($result ? 'OK' : 'FAIL'));
		return [
			'status' => $result
		];
	}
	
	protected function initPostJson()
	{
		if($_SERVER['REQUEST_METHOD'] !== 'GET'){
			if(!$_POST || !count($_POST)){
				$_POST = json_decode(file_get_contents('php://input'), true);
				if(json_last_error() !== JSON_ERROR_NONE){
					$this->info('JSON DECODE ERROR: ' . json_last_error_msg());
				}
			}
		}
	}
	
	protected function bootEngine()
	{
		if($this->engine_kernel) {
			return $this->engine_kernel;
		}
		$autoload = $this->base_path . '/vendor/autoload.php';
		if (!file_exists($autoload)) {
			throw new InstallerException('Unable to find autoloader: ~/vendor/autoload.php');
		}
		require_once $autoload;
		
		$bootstrap_app = $this->base_path . '/bootstrap/app.php';
		if (!file_exists($bootstrap_app))
			throw new InstallerException('Unable to find app loader: ~/bootstrap/app.php');
		
		$app = require_once $bootstrap_app;
		$this->engine = $app;
		$kernel = $app->make('Illuminate\Contracts\Console\Kernel');
		//$kernel->bootstrap();
		$this->engine_kernel = $kernel;
		return $this->engine_kernel;
	}
	
	/**
	 * @param  string  $key
	 * @param  mixed|null  $default
	 * @return mixed|null
	 */
	protected function post(string $key, $default = null)
	{
		return array_key_exists($key, $_POST) ? $_POST[$key] : $default;
	}
	
	protected function e($value)
	{
		return htmlentities($value, ENT_QUOTES, 'UTF-8', false);
	}
	
	public function getBaseUrl()
	{
		if (isset($_SERVER['HTTP_HOST'])) {
			$baseUrl = !empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off' ? 'https' : 'http';
			$baseUrl .= '://'. $_SERVER['HTTP_HOST'];
		}
		else {
			$baseUrl = 'http://localhost/';
		}
		return $baseUrl;
	}
	
	protected function initLogger()
	{
		$this->log_file = 'install.log';
		$this->logger = new Logger($this->installer_files_path, 'debug', [
			'filename' => $this->log_file,
		]);
		if($_SERVER['REQUEST_METHOD'] === 'GET'){
			$this->cleanLog();
		}
		//$this->cleanLog();
	}
	
	public function cleanLog()
	{
		$filepath = $this->installer_files_path . DIRECTORY_SEPARATOR . $this->log_file;
		file_put_contents($filepath, '');
		
		$header = [
			'================================================================',
			' ________   _______ .__   __.   _______  __  .__   __.  _______ ',
			'|       /  |   ____||  \ |  |  /  _____||  | |  \ |  | |   ____|',
			'`---/  /   |  |__   |   \|  | |  |  __  |  | |   \|  | |  |__   ',
			'   /  /    |   __|  |  . `  | |  | |_ | |  | |  . `  | |   __|  ',
			'  /  /----.|  |____ |  |\   | |  |__| | |  | |  |\   | |  |____ ',
			' /________||_______||__| \__|  \______| |__| |__| \__| |_______|',
			'',
			'======================= Installation Log =======================',
		];
		$this->logger->write(implode(PHP_EOL, $header).PHP_EOL);
	}
	
	protected function logPostRequest(){
		if (!isset($_POST) || !count($_POST)) return;
		$data = $_POST;
		$this->logger->write('=================== POST REQUEST ==================='.PHP_EOL);
		$this->logVar($data);
	}
	
	protected function logVar($data)
	{
		$this->info(print_r($data, 1));
	}
	
	public function info($message, $var = null)
	{
		$this->logger->info($message, $var);
	}
}
<?php

if(""===session_id()){
	session_start();
}
/*
 * Required PHP version for October CMS
 */
define('REQUIRED_PHP_VERSION', '7.1.3');

if (version_compare(strtolower(trim(PHP_VERSION)), REQUIRED_PHP_VERSION, '<')) {
	exit('PHP version 7.1.3 or above is required to install ZEngine.');
}

/*
 * Check for JSON extension
 */
if (!function_exists('json_decode')) {
	exit('JSON PHP Extension is required to install ZEngine.');
}

/*
 * PHP headers
 */
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', false);
header('Pragma: no-cache');

// Check debug
$isDebug = array_key_exists('debug', $_GET);
ini_set('display_errors', (int) $isDebug);
error_reporting( (int) $isDebug);
// Check dev
$isDev = array_key_exists('dev', $_GET);

/*
 * Constants
 */
define('BASE_PATH', str_replace("\\", "/", dirname(__DIR__, 2)));
/*
 * Address timeout limits
 */
@set_time_limit(3600);

register_shutdown_function('installerShutdown');
function installerShutdown()
{
	global $installer;
	$error = error_get_last();
	if ($error && $error['type'] == 1) {
		header('HTTP/1.1 500 Internal Server Error', true, 500);
		$errorMsg = htmlspecialchars_decode(strip_tags($error['message']));
		echo $errorMsg;
		if (isset($installer)) {
			$installer->info(sprintf('Fatal error: %s on line %s in file %s', $errorMsg, $error['line'], $error['file']));
		}
		exit;
	}
}

require_once 'InstallerException.php';
require_once 'ConfigRewriter.php';
require_once 'Logger.php';
require_once 'Downloader.php';
require_once 'Installer.php';

try{
	$installer = new Installer();
}catch (Exception $exception){
	header('HTTP/1.1 500 Internal Server Error', true, 500);
	if(isset($installer) && $installer instanceof Installer){
		$installer->info('Fatal error: ', $exception->getMessage());
		$installer->info('Error trace log', $exception->getTraceAsString());
	}
	$errorMsg = htmlspecialchars_decode(strip_tags($exception->getMessage()));
	echo $errorMsg;
	exit;
}

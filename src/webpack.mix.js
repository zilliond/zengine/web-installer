const mix = require('laravel-mix');
const tailwindcss = require('tailwindcss')

mix.setPublicPath('../install_files');
mix.sourceMaps();

mix.js('js/app.js', 'js');

mix.sass('scss/app.scss', 'css')
		.options({
			processCssUrls: false,
			postCss: [ tailwindcss('./tailwind.config.js') ],
		});
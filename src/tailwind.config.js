module.exports = {
	future: {
		// removeDeprecatedGapUtilities: true,
		// purgeLayersByDefault: true,
	},
	purge: [],
	theme: {
		extend: {
			fontFamily: {
				display: ['SFUI Display', 'sans-serif'],
				body: ['Rubik', 'sans-serif'],
			},
			colors: {
				darkgray: '#323232',
				gray: '#505050',
				blue: '#1f69a8',
				error: '#ad1705',
			},
			boxShadow: {
				input_blue: '0px 0px 5px 0px #1f69a8',
				input_red: '0px 0px 5px 0px #ad1705',
			}
		},
	},
	variants: {},
	plugins: [],
}

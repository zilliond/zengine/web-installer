export default {
	Database: [
		{
			label: 'Mysql host',
			key: 'db_host',
			type: 'text',
		},
		{
			label: 'Mysql port',
			key: 'db_port',
			type: 'text',
			is_int: true,
		},
		{
			label: 'Database Name',
			key: 'db_name',
			type: 'text',
		},
		{
			label: 'Mysql login',
			key: 'db_user',
			type: 'text',
		},
		{
			label: 'Mysql password',
			key: 'db_password',
			type: 'password',
		},
	],
	Admin: [
		{
			label: 'Admin name',
			key: 'admin_name',
			type: 'text',
		},
		{
			label: 'Admin email',
			key: 'admin_email',
			type: 'email',
		},
		{
			label: 'Admin login',
			key: 'admin_login',
			type: 'text',
		},
		{
			label: 'Admin password',
			key: 'admin_password',
			type: 'password',
		},
		{
			label: 'Admin password confirmation',
			key: 'admin_password_confirmation',
			type: 'password',
		},
	],
	Engine: [
		{
			label: 'Site name',
			key: 'engine_name',
			type: 'text',
		},
		{
			label: 'Encryption key (32bit)',
			key: 'engine_key',
			type: 'text',
		},
		{
			label: 'ZEngine licence',
			key: 'engine_licence',
			type: 'textarea',
		},
		{
			label: 'Balances type',
			key: 'engine_balances',
			type: 'select',
			options: {
				divided: 'Divided',
				shared: 'Shared',
			}
		},
	]

}
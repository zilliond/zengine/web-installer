import _ from 'lodash';
import axios from "axios";

const config = {
	namespaced: true,
	state: () => ({
		installing: false,
		current_label: 'Install ?',
		label_error: false,
		current_stage_index: null,
		stages: [
			{
				action: 'downloadEngine',
				start_label: 'Downloading Engine...',
				success_label: 'Engine downloaded',
				result: null,
				progress: null,
				is_started: false,
				is_finished: false,
				is_success: null,
				is_error: null,
				error: null
			},
			{
				action: 'extractEngine',
				start_label: 'Extracting Engine...',
				success_label: 'Engine extracted',
				result: null,
				progress: null,
				is_started: false,
				is_finished: false,
				is_success: null,
				is_error: null,
				error: null
			},
			{
				action: 'configDatabase',
				start_label: 'Database configuring...',
				success_label: 'Database configured',
				config: 'database_config', // Config module getter
				result: null,
				progress: null,
				is_started: false,
				is_finished: false,
				is_success: null,
				is_error: null,
				error: null
			},
			{
				action: 'configEngine',
				start_label: 'Engine configuring...',
				success_label: 'Engine configured',
				config: 'engine_config', // Config module getter
				result: null,
				progress: null,
				is_started: false,
				is_finished: false,
				is_success: null,
				is_error: null,
				error: null
			},
			{
				action: 'setupDatabase',
				start_label: 'Database setup...',
				success_label: 'Database setup final',
				result: null,
				progress: null,
				is_started: false,
				is_finished: false,
				is_success: null,
				is_error: null,
				error: null
			},
			{
				action: 'createAdmin',
				start_label: 'Creating Admin...',
				success_label: 'Admin created',
				config: 'admin_config', // Config module getter
				result: null,
				progress: null,
				is_started: false,
				is_finished: false,
				is_success: null,
				is_error: null,
				error: null
			},
			{
				action: 'cleanInstaller',
				start_label: 'Cleaning installer files...',
				success_label: 'installer files deleted!',
				result: null,
				progress: null,
				is_started: false,
				is_finished: false,
				is_success: null,
				is_error: null,
				error: null
			},
		]
	}),
	getters: {
		progress: state => {
			let stages_count = state.stages.length;
			let finished = state.stages.reduce((carry, stage) => {
				if(stage.is_finished){
					return carry + 1;
				}else if(stage.progress){
					return carry + (stage.progress / 100);
				}
				return carry;
			}, 0)
			return ((finished / stages_count) * 100).toFixed(0);
		},
		installing: state => state.installing,
		currentLabel: state => state.current_label,
		stages: state => state.stages,
		stageByIndex: state => index => state.stages[index],
		stageByAction: state => stageName => state.stages.find(stage => stage.stage === stageName),
		current_stage_index: state => state.current_stage_index,
		current_stage: state => state.stages[state.current_stage_index],
		prev_stage: state => state.current_stage_index > 0 ? state.stages[state.current_stage_index - 1] : null,
		next_stage_index: state => {
			if (state.current_stage_index === null) {
				return 0;
			}
			return state.current_stage_index < (state.stages.length - 1) ? state.current_stage_index + 1 : null;
		},
		is_error: state => state.stages.reduce((is_error, stage) => is_error || stage.is_error, false),
		is_success: state => state.stages.reduce((is_success, stage) => is_success && stage.is_success, true),
	},
	mutations: {
		set_installing(state, installing){
			state.installing = installing;
		},
		set_current_label(state, label){
			state.current_label = label;
		},
		set_stage_data(state, payload){
			if(Number.isInteger(payload.stage) && payload.data){
				state.stages[payload.stage] = Object.assign(state.stages[payload.stage], payload.data);
			}
		},
		set_current_stage_index(state, index){
			if(index < 0 || !Number.isInteger(index)){
				state.current_stage_index = null;
			}else if(index > (state.stages.length - 1)){
				state.current_stage_index = state.stages.length - 1;
			}
			state.current_stage_index = index;
		},
	},
	actions: {
		install({commit, dispatch}){
			commit('set_installing', true);
			dispatch('initInstall');
		},
		initInstall({dispatch}){
			dispatch('startStage', 0);
		},
		startStage({getters, commit, dispatch}, stageIndex){
			commit('set_current_stage_index', stageIndex);
			let stage = getters.stageByIndex(stageIndex);
			commit('set_current_label', stage.start_label);
			commit('set_stage_data', {
				stage: stageIndex,
				data:{
					is_started: true,
				}
			});
			if(stage.action){
				if(stage.action === 'downloadEngine'){
					dispatch('downloadProgress', stageIndex);
				}
				dispatch('stageAction', stageIndex);
			}
		},
		stageAction({getters, commit, dispatch, rootGetters}, stageIndex){
			let stage = getters.stageByIndex(stageIndex);
			let data = {};
			console.log(stage.config);
			if(stage.config){
				console.log('config/'+stage.config);
				data = rootGetters['config/'+stage.config];
				console.log(rootGetters['config/'+stage.config]);
			}
			axios.post('/install.php', {
				handler: 'onInstall',
				action: stage.action,
				data: data
			})
				.then(({data}) => {
					console.log(data);
					if(data.status === 'success'){
						commit('set_current_label', stage.success_label);
						commit('set_stage_data', {
							stage: stageIndex,
							data: {
								is_success: true,
								is_error: false,
								is_finished: true,
								result: data,
							}
						});
						console.log(getters.next_stage_index);
						if(getters.next_stage_index){
							setTimeout(() => dispatch('startStage', getters.next_stage_index), 500);
						}else{
							if(getters.is_success){
								commit('set_current_label', 'Engine installed, redirect to site after 1s');
								setTimeout(() => {
									window.location.href = '/';
								}, 1000);
							}
						}
					}
				}).catch(error => {
					console.log(error);
					console.log(error.response);
					if (error.response) {
						commit('set_current_label', 'Error: '+ error.response.data.message);
						commit('set_stage_data', {
							stage: stageIndex,
							data: {
								is_error: true,
							}
						});
					}else{
						commit('set_current_label', 'Error: Unexpected Error');
						commit('set_stage_data', {
							stage: stageIndex,
							data: {
								is_error: true,
							}
						});
					}
				});
		},
		downloadProgress({commit, dispatch}, stageIndex){
			commit('set_stage_data', {
				stage: stageIndex,
				data: {
					progress: 0,
				}
			});
			dispatch('_downloadProgress', stageIndex);
		},
		_downloadProgress({commit, dispatch}, stageIndex){
			setTimeout(() => {
				axios.post('/install.php', {
					handler: 'onInstall',
					action: 'downloadProgress',
					data: null
				})
					.then(({data}) => {
						if(data.status === 'progress'){
							commit('set_stage_data', {
								stage: stageIndex,
								data: {
									progress: Number.parseInt(data.progress),
								}
							});
							dispatch('_downloadProgress', stageIndex)
						}else if(data.status === 'success'){
							commit('set_stage_data', {
								stage: stageIndex,
								data: {
									progress: 100,
								}
							});
						}
					})
			}, 200)
		}
	},
}

export default config;

import _ from 'lodash';
import axios from "axios";
import settings_fields from "../../settings_fields";
const config = {
	namespaced: true,
	state: () => ({
  		tab: 0,
		tabs: [
			{
				label: 'Database',
				component: 'Database'
			},
			{
				label: 'Admin',
				component: 'Admin'
			},
			{
				label: 'Engine',
				component: 'Engine'
			}
		],
		settings: {
			engine_name: 'Zengine',
  			engine_key: '9fFx495HjKD99d7Y4QW6gveU83AiYLtc',
			engine_licence: null,
			engine_balances: 'shared',
			db_host: 'localhost',
			db_port: 3306,
			db_name: 'zengine',
			db_user: 'root',
			db_password: '',
			admin_name: 'Admin',
			admin_email: 'admin@site.com',
			admin_login: 'admin',
			admin_password: null,
			admin_password_confirmation: null,
		},
		validation: {
  			Database: {
  				status: null,
				message: null
			},
  			Admin: {
  				status: null,
				message: null,
			},
  			Engine: {
  				status: null,
				message: null,
			},
		}
	}),
	getters: {
		tab: state => state.tab,
		currentTab: state => state.tabs[state.tab],
		tabs: state => state.tabs,
		settings: state => state.settings,
		setting: state => key => state.settings[key],
		validations: state => state.validations,
		database_config: state => _.pickBy(state.settings, (value, key) => _.startsWith(key, 'db_')),
		database_validation: state => state.validation.Database,
		admin_config: state => _.pickBy(state.settings, (value, key) => _.startsWith(key, 'admin_')),
		admin_validation: state => state.validation.Admin,
		engine_config: state => _.pickBy(state.settings, (value, key) => _.startsWith(key, 'engine_')),
		engine_validation: state => state.validation.Engine,
		validated: state => _.reduce(state.validation, (valid, validation) => valid && (validation.status === 'success'), true)
	},
	mutations: {
		tab_set(state, tab){
			state.tab = tab;
		},
		tab_prev(state){
			state.tab = state.tab > 0 ? state.tab - 1 : 0;
		},
		tab_next(state){
			const maxIndex = state.tabs.length - 1;
			state.tab = state.tab < maxIndex ? state.tab + 1 : maxIndex;
		},
		setting_set(state, payload){
			if(state.settings.hasOwnProperty(payload.key)){
				state.settings[payload.key] = payload.value;
			}
		},
		validation_set(state, payload){
			if(state.validation.hasOwnProperty(payload.tab)){
				state.validation[payload.tab] = payload.validation;
			}
		}
	},
	actions: {
		validateDatabase({getters, commit, dispatch}){
			axios.post('/install.php', {
				handler: 'onCheckDatabaseConnection',
				payload: getters.database_config
			})
				.then(({data}) => {
					commit('validation_set', { tab: 'Database',
						validation: {
							status: data.status ? 'success' : 'error',
							message: data.message
						}});
					dispatch('validateAdmin');
					dispatch('validateEngine');
				})
				.catch(error => {
					if (error.response) {
						commit('validation_set', {
							tab: 'Database',
							validation: error.response.data
						});
					} else if (error.request) {
						commit('validation_set', { tab: 'Database',
							validation: {
								status: 'error',
								message: 'The request was made but no response was received'
							}});
						console.log(error.request);
					} else {
						commit('validation_set', { tab: 'Database',
							validation: {
								status: 'error',
								message: error.message
							}});
					}
				});
		},
		validateAdmin({getters, commit}){
			let is_error = false;
			let message = '';
			if(!getters.admin_config.admin_name.length){
				is_error = true;
				message += 'Please, fill name field\n';
			}
			if(!getters.admin_config.admin_login.length){
				is_error = true;
				message += 'Please, fill login field\n';
			}
			if(!(/(.+)@(.+){2,}\.(.+){2,}/.test(getters.admin_config.admin_email))){
				is_error = true;
				message += 'Email invalid\n';
			}
			if(!getters.admin_config.admin_password || !getters.admin_config.admin_password.length){
				is_error = true;
				message += 'Please, fill password field\n';
			}
			if(getters.admin_config.admin_password !== getters.admin_config.admin_password_confirmation){
				is_error = true;
				message += 'Passwords not match';
			}
			commit('validation_set', {
				tab: 'Admin',
				validation: {
					status: is_error ? 'error' : 'success',
					message: message ? message : 'User valid'
				}});
		},
		validateEngine({getters, commit}){
			let is_error = false;
			let message = '';
			if(!getters.engine_config.engine_name.length){
				is_error = true;
				message += 'Please, fill name field\n';
			}
			if(getters.engine_config.engine_key.length !== 32){
				is_error = true;
				message += 'Encryption key must be of a valid length(32)\n';
			}
			if(['shared', 'divided'].indexOf(getters.engine_config.engine_balances) === -1){
				is_error = true;
				message += 'You need to choose balances type\n';
			}
			commit('validation_set', {
				tab: 'Engine',
				validation: {
					status: is_error ? 'error' : 'success',
					message: message ? message : 'Valid'
				}});
		}
	},
}

export default config;

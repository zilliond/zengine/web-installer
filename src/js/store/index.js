import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from "vuex-persistedstate";
import config from "./modules/config";
import install from './modules/install';
import _ from "lodash";
Vue.use(Vuex);

const state = {
	step: 1,
	steps: [
		{
			id: 1,
			title: 'Requirements',
			component: 'check-step',
		},
		{
			id: 2,
			title: 'Configuration',
			component: 'configuration-step',
		},
		{
			id: 3,
			title: 'Installing',
			component: 'install-step',
		},
	],
	requirements: [
		{
			type: 'php',
			label: 'PHP version 7.1.3 or greater required',
			status: null
		},
		{
			type: 'ioncube',
			label: 'Ioncube PHP Extension is required',
			status: null
		},
		{
			type: 'curl',
			label: 'cURL PHP Extension is required',
			status: null
		},
		{
			type: 'json',
			label: 'JSON PHP Extension is required',
			status: null
		},
		{
			type: 'pdo',
			label: 'PDO PHP Extension is required',
			status: null
		},
		{
			type: 'mbstring',
			label: 'Mbstring PHP Extension is required',
			status: null
		},
		{
			type: 'fileinfo',
			label: 'Fileinfo PHP Extension is required',
			status: null
		},
		{
			type: 'openssl',
			label: 'OpenSSL PHP Extension is required',
			status: null
		},
		{
			type: 'gd',
			label: 'GD PHP Extension is required',
			status: null
		},
		{
			type: 'zip',
			label: 'ZipArchive PHP Extension is required',
			status: null
		},
		{
			type: 'hash',
			label: 'Hash PHP Extension is required',
			status: null
		},
		{
			type: 'soap',
			label: 'Soap PHP Extension is required',
			status: null
		},
		{
			type: 'zillion_storage',
			label: 'Check connection to Zillion storage',
			status: null
		},
	]
};

const getters = {
	step: state => state.step,
	currentStepData: state => state.steps.find(step => state.step === step.id),
	steps: state => state.steps,
	requirements: state => state.requirements,
	requirement: state => type => state.requirements.find(requirement => type === requirement.type),
	canGoToStep: state => step => {
		if(step === 1){
			return true;
		}
		if(step === 2){
			return state.requirements.reduce((carry, item) => carry && (item.status === 'success'), true);
		}
		if(step === 3){
			return _.reduce(state.config.validation, (valid, validation) => valid && (validation.status === 'success'), true);
		}
		return false;
	},
	canNext: (state, getters) => getters.canGoToStep(state.step + 1),
};

const actions = {
	setStep({commit, getters}, step){
		if(getters.canGoToStep(step)){
			commit('SET_STEP', step);
		}
	},
	prevStep({commit}){
		commit('PREV_STEP');
	},
	nextStep({commit}){
		commit('NEXT_STEP');
	},
	setRequirementStatus({commit}, payload){
		commit('SET_REQUIREMENT_STATUS', payload);
	}
};

const mutations = {
	SET_STEP(state, step){
		state.step = step;
	},
	PREV_STEP(state){
		state.step = state.step > 1 ? state.step - 1 : 1;
	},
	NEXT_STEP(state){
		state.step = state.step < state.steps.length ? state.step + 1 : state.steps.length;
	},
	SET_REQUIREMENT_STATUS(state, payload){
		let requirement = state.requirements.find(item => item.type === payload.type);
		requirement.status = payload.status;
		state.requirements = [
			...state.requirements.map(item => item.type === payload.type ? {...requirement} : item)
		];
	},
};
let plugins = [];
const params = new URLSearchParams(window.location.search)
if(params.has('debug')){
    plugins.push(
        createPersistedState({
            paths: [
                'step',
                'steps',
                'requirements',
                'config.settings',
                'config.tab',
            ]
        })
    );
}
const store = new Vuex.Store({
	modules: {
		config,
		install
	},
	state,
	getters,
	actions,
	mutations,
	plugins: plugins,
});
store.subscribe((mutation, state) => {
	if(mutation.type === 'config/setting_set'){
		console.log(state.config.settings);
	}
});
export default store;

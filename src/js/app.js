import Vue from "vue";
import store from "./store/index";

import App from "./App";
import CheckStep from "./steps/CheckStep";
import ConfigurationStep from "./steps/ConfigurationStep";
import InstallStep from "./steps/InstallStep";
Vue.component('app', App);
Vue.component('check-step', CheckStep);
Vue.component('configuration-step', ConfigurationStep);
Vue.component('install-step', InstallStep);


const app = new Vue({
	el: '#app',
	store,
})